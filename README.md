# The External Drift Kriging -- EDK program

This repository contains the external drift kriging (EDK) Fortran program developed at the Dept. Computational Hydrosystems at the Helmholtz Centre for Environmental Research - UFZ.

The EDK comes with a [LICENSE][1] agreement, this includes also the GNU Lesser General Public License.

**Please note**: The GitLab repository grants read access to the code.
If you like to contribute to the code, please contact stephan.thober@ufz.de.

## Installation

Installation instructions can be found in [INSTALL][2] for Windows, MacOS, and GNU/Linux distributions.

## Cite as

Please refer to the EDK algorithm by citing Samaniego et al. (2011). EDK aplications in Samaniego et al. (2013) or Zink et al. (2017).
Rainfall network design and EDK cross-validation in Zacharias, S. et al. (2011).

- Samaniego, L., R. Kumar, and C. Jackisch (2011), "Predictions in a data-sparse region using a regionalized grid-based hydrologic model driven by remotely sensed data", Hydrology research, 42(5), 338–355, doi:10.2166/nh.2011.156.
- Samaniego, L. R. Kumar, M. Zink (2013), "Implications of Parameter Uncertainty on Soil Moisture Drought Analysis in Germany", J Hydrometeor, 2013 vol. 14 (1) pp. 47-68. http://journals.ametsoc.org/doi/abs/10.1175/JHM-D-12-075.1
- Zink, M., R. Kumar, M. Cuntz, and L. Samaniego (2017), "A high-resolution dataset of water fluxes and states for Germany accounting for parametric uncertainty", Hydrol. Earth Syst. Sci., 21(3), 1769–1790, doi:10.5194/hess-21-1769-2017.
- Zacharias, S., H. Bogena, L. Samaniego et al. (2011), "A Network of Terrestrial Environmental Observatories in Germany", Vadose Zone Journal, 10(3), 955, doi:10.2136/vzj2010.0139.

[1]: LICENSE
[2]: INSTALL.md
